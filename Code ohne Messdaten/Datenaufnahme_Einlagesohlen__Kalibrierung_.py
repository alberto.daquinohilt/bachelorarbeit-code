"""
Created on Tue Nov 24 08:26:34 2022
@author: Alberto d'Aquino Hilt
"""
import time
import serial
import matplotlib.pyplot as plt 
import numpy as np
import os

logdir = '16bit/Messdaten_100kg/'
ser = serial.Serial(port='COM10',
                    baudrate=500000,
                    timeout=1,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS)
ser.isOpen()

# Create Timestamp + New Folder 
timestamp = time.asctime(time.localtime())
timestamp = timestamp.replace(':', '-')
directory = logdir + timestamp + '/'
if not os.path.exists(directory):
   os.makedirs(directory)
   
print(time.time())
try : 
    messdaten = np.empty([1,8])
    messzeiten = []
    time_begin = time.time()
    data = ''
    print('Start')
    while 1 :
        bytesToRead = ser.inWaiting()
        new_data = ser.read(bytesToRead)
        data = data + new_data.decode()
        print(data)
        if '\r\n' in data : 
            messaufteilung = data.split(',\r\n') 
            messung_str = messaufteilung[0] #['S0:2,S1:3,...'] 
            sensormessliste_str = messung_str.split(',') #['S0:2','S1:3',...] 
            messung_int = [] #[2,3,0,...]
            for sensor in sensormessliste_str:
                einzelmessung_int = int(sensor.split(':')[1])
                messung_int.append(einzelmessung_int)
            messdaten = np.append(messdaten,[messung_int],axis=0)      
            messzeiten.append(time.time() - time_begin)
            data = data[len(messung_str) + 3:]         
        time.sleep(0.01)
except (Exception, serial.SerialException) as e : 
    print(e)
    ser.close()

except (KeyboardInterrupt) as e : 
    messdaten = messdaten[1:,:]
    plt.figure(figsize=(10,7))
    for i in range(8) :
        plt.plot(messzeiten, messdaten[:,i], label=f'S{i}') 
    plt.xlabel('Zeit [s]')
    plt.ylabel('Rel. Unit')
    plt.title('Druckmessung(Arduino)')
    plt.legend()
    plt.grid(True)
    print(directory)
    plt.savefig(directory + f'Plot_Arduino.png',dpi=250)
    # Liveplots auf https://pythonprogramming.net/live-graphs-matplotlib-tutorial/
    
    # Zeiten hinzufügen 
    mz_array = np.array(messzeiten).reshape(-1, 1)
    abs_time = mz_array + time_begin
    messdaten_timestamp = np.hstack([messdaten, abs_time])
    
    # als csv speichern 
    np.savetxt(directory + f'Messdaten_Arduino.csv', messdaten_timestamp, delimiter=',')
    
    # Textdatei mit Timestamp-Name hinzufügen:
    # np.savetxt(logdir + f'{timestamp}')
    
ser.close()
