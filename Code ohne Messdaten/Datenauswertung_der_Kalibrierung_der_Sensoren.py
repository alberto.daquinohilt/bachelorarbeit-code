"""
Created on Tue Dec 11 15:56:47 2022
@author: Alberto d'Aquino Hilt
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime

start_time = datetime.now()
print(start_time)

# path for comparation of sensors
path = r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten'

# root for calibration, repeatiability, mad and regression
root = [r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s0',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s1',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s2',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s3',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s4',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s5',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s6',
        r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Material 0_s7']

# define paths of all nine measurements of all eight sensors
ards = [r'\Fri Dec  9 13-14-18 2022', r'\Fri Dec  9 13-19-28 2022', r'\Fri Dec  9 13-21-45 2022', # sensor 0
        r'\Fri Dec  9 13-23-58 2022', r'\Fri Dec  9 13-26-12 2022', r'\Fri Dec  9 13-28-20 2022',
        r'\Fri Dec  9 13-32-50 2022', r'\Fri Dec  9 13-35-00 2022', r'\Fri Dec  9 13-41-22 2022',
        r'\Fri Dec  9 14-26-27 2022', r'\Fri Dec  9 14-35-15 2022', r'\Fri Dec  9 14-38-01 2022', # sensor 1
        r'\Fri Dec  9 14-43-14 2022', r'\Fri Dec  9 14-45-30 2022', r'\Fri Dec  9 14-47-40 2022',
        r'\Fri Dec  9 14-49-50 2022', r'\Fri Dec  9 14-53-07 2022', r'\Fri Dec  9 15-00-14 2022',
        r'\Fri Dec  9 15-06-43 2022', r'\Fri Dec  9 15-09-44 2022', r'\Fri Dec  9 15-12-24 2022', # sensor 2
        r'\Fri Dec  9 15-14-42 2022', r'\Fri Dec  9 15-18-33 2022', r'\Fri Dec  9 15-22-53 2022',
        r'\Fri Dec  9 15-26-00 2022', r'\Fri Dec  9 15-28-50 2022', r'\Fri Dec  9 15-33-07 2022',
        r'\Sat Dec 10 11-10-35 2022', r'\Sat Dec 10 11-24-17 2022', r'\Sat Dec 10 11-27-12 2022', # sensor 3
        r'\Sat Dec 10 11-29-37 2022', r'\Sat Dec 10 11-32-14 2022', r'\Sat Dec 10 11-37-12 2022',
        r'\Sat Dec 10 11-40-14 2022', r'\Sat Dec 10 11-44-00 2022', r'\Sat Dec 10 11-46-23 2022',
        r'\Fri Dec  9 15-43-24 2022', r'\Fri Dec  9 15-45-57 2022', r'\Fri Dec  9 15-49-00 2022', # sensor 4
        r'\Fri Dec  9 15-51-29 2022', r'\Fri Dec  9 15-55-14 2022', r'\Fri Dec  9 15-57-51 2022',
        r'\Fri Dec  9 16-00-36 2022', r'\Fri Dec  9 16-03-14 2022', r'\Fri Dec  9 16-05-26 2022',
        r'\Sat Dec 10 11-59-59 2022', r'\Sat Dec 10 12-02-48 2022', r'\Sat Dec 10 12-05-27 2022', # sensor 5
        r'\Sat Dec 10 12-08-06 2022', r'\Sat Dec 10 12-11-12 2022', r'\Sat Dec 10 12-27-28 2022',
        r'\Sat Dec 10 12-30-03 2022', r'\Sat Dec 10 12-33-30 2022', r'\Sat Dec 10 12-36-09 2022',
        r'\Mon Dec 12 13-21-46 2022', r'\Mon Dec 12 13-24-21 2022', r'\Mon Dec 12 13-28-17 2022', # sensor 6
        r'\Mon Dec 12 13-30-41 2022', r'\Mon Dec 12 13-34-12 2022', r'\Mon Dec 12 13-36-50 2022',
        r'\Mon Dec 12 13-42-41 2022', r'\Mon Dec 12 13-52-53 2022', r'\Mon Dec 12 13-55-17 2022',
        r'\Mon Dec 12 18-56-40 2022', r'\Mon Dec 12 19-02-32 2022', r'\Mon Dec 12 19-10-51 2022', # sensor 7
        r'\Mon Dec 12 19-15-35 2022', r'\Mon Dec 12 19-18-05 2022', r'\Mon Dec 12 19-21-42 2022',
        r'\Mon Dec 12 19-27-15 2022', r'\Mon Dec 12 19-30-03 2022', r'\Mon Dec 12 19-33-45 2022']

# open dataframe for all datapoints and all parameters of regression
df_reg_params = pd.DataFrame()
df_reg_ges = pd.DataFrame()
df_cal = pd.DataFrame()
df_cal_ges= pd.DataFrame()
data = [[] for i in range(8)]

# iterate through all eight sensors/roots
for j in range(len(root)):
    
    # open lists
    messungen_liste = []
    messungen_mean = []
    messungen_std = []
    
    # create dataframes for mad
    df_mad_ard = pd.DataFrame(np.nan, index = list(range(981)), columns = ['ard_0', 'ard_1', 'ard_2', 
                                                                            'ard_3', 'ard_4', 'ard_5', 
                                                                            'ard_6', 'ard_7'])
    df_mad_fg = pd.DataFrame(np.nan, index = list(range(981)), columns = ['fg_0', 'fg_1', 'fg_2', 
                                                                           'fg_3', 'fg_4', 'fg_5', 
                                                                           'fg_6', 'fg_7'])

    # iterate through all nine measurements of every sensor
    for i in range(9):
        k = i + 9*j
        
        # open dataframes and read data
        col_names_ard = ['s0', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'time']
        col_names_fg = ['fg', 'time']
        df_ard = pd.read_csv(root[j] + ards[k] + '\Messdaten_Arduino.csv', names = col_names_ard)
        df_fg = pd.read_csv(root[j] + ards[k] + '\Messdaten_FG.csv', names = col_names_fg)
        
        # find used sensor
        u_s_idx = df_ard.drop(['time'], axis = 1).mean().idxmax()
        df_ard_used = df_ard[[u_s_idx, 'time']]
        df_ard_used.rename(columns = {u_s_idx:'ard','time':'time'}, inplace = True)
    
        # dataframe with time (index), ard and fg (NaN in holes)
        df_ard_used['fg'] = np.nan
        df_fg['ard'] = np.nan
        df_ges = pd.concat([df_ard_used, df_fg])
        df_ges = df_ges.sort_values('time')
    
        # save dataframe as datetime for interpolation
            # index of datetime is date and exact hour of measurement
        datetimes = pd.to_datetime(df_ges['time'].values, unit = 's')
        df_ges.index = pd.DatetimeIndex(data = datetimes)
        
        # interpolate
        df_ges['fg'] = df_ges['fg'].interpolate(method = 'time')
        df_ges['ard'] = df_ges['ard'].interpolate(method = 'time')
        
        # remove all rows with NaN -> ignore df_ges_useless
        df_ges_useless = df_ges.dropna(inplace = True)
    
        # remove all fg-values over 100,5
        df_ges.drop(df_ges[df_ges['fg'] > 100].index, inplace = True)
        
        # calculate time in sek from start
        df_ges['time'] = df_ges['time'] - df_ges.iloc[0,1]
        df_ges = df_ges.reset_index(drop = True)
        
        # MAD: create groups of 981 N for every measurement and take the mean of every group
        df_group = df_ges.groupby(pd.cut(df_ges['fg'], np.arange(0, 101, 1)), axis=0)
        df_m = df_group.mean()
        df_std = df_group.std()
        
        # MESSW: append all groups to list
        messungen_mean.append(df_m)
        messungen_std.append(df_std)
        
        # append measurements in list 
        messungen_liste.append(df_ges)

    # save all measurements in one list into dataframe
    df_mess_ges = pd.concat(messungen_liste) # for regression
    df_mess_ges['fg'] = df_mess_ges['fg']*9.81
    df_calcurve = df_mess_ges # for calibration and for mad
    df_mess_ges_m = pd.concat(messungen_mean)
    df_mess_ges_s = pd.concat(messungen_std)
    
# CALIBRATIONCURVE
    
    # sort by time for rawdata plot 
    df_time = df_calcurve.sort_values('time')
    
    # add 0-row to end and roll to start of dataframe
    df_calcurve = df_calcurve.append(pd.Series(0, index = df_calcurve.columns), ignore_index = True).apply(np.roll, shift = 1)

    # create groups of all 9 measurements together
    df_group = df_calcurve.groupby(pd.cut(df_calcurve['fg'], np.arange(0, 981, 1)), axis=0)
        # .describe() and df_group.count().min() for smallest group
    
    # mean of every group and reset index of dataframe 
    df_m = df_group.mean()
    df_m = df_m.reset_index(drop = True)
    
    # standard deviation of all groups and reset index of dataframe 
    df_std = df_group.std().reset_index(drop = True)

    # rest index of dataframe for rawdata plots
    df_time = df_m.reset_index(drop = True)

    # delete time column
    df_m = df_m.drop('time', axis = 1)
    df_std = df_std.drop('time', axis = 1)
    
    # create 0-dataframe, rename columns and set to start of dataframe
    df_row = pd.DataFrame(0, index = list(range(1)), columns = ['ard','fg'])
    df_m = pd.concat([df_row, df_m], axis = 0, ignore_index = True)
    df_std = pd.concat([df_row, df_std], axis = 0, ignore_index = True)

    # merge df_m and df_std for statistics and save as csv
    df_stats_grp = pd.concat([df_m, df_std], axis=1)
    df_stats_grp.columns=['ard_m', 'fg_m', 'ard_s', 'fg_s']
    df_stats_grp.to_csv(root[j] + '\Kalibrierungskurve_mit_stat.csv')
    
    # add 0-row to end of dataframe and roll to first row
    df_time = df_time.sort_values('time')
    df_time = df_time.append(pd.Series(0, index = df_time.columns), ignore_index = True).apply(np.roll, shift = 1)
    
    # rawdata plot of force gauge only
    plt.figure(figsize = (10, 7))
    plt.plot(df_time['time'], df_time['fg'], linewidth = '1')
    plt.xlabel('Zeit [s]', fontdict = {'size':12})
    plt.ylabel('VBRK [N]', fontdict = {'size':12}) 
    plt.title(f'Rohdaten des Kraftmessgerätes (Sensor {j})', fontdict = {'size':16})
    plt.xlim(0, 120)
    plt.ylim(0, 981)
    plt.grid()
    plt.tight_layout(pad = 2.0)
    plt.savefig(root[j] + '\Kraftmessgerät.pdf')
    plt.show()

    # plot calibrationcurve with mean and (multiple) standard deviations of ard
    plt.figure(figsize = (10, 7))
    plt.plot(df_m['ard'], df_m['fg'])
    plt.fill_betweenx(df_m['fg'], df_m['ard'] - df_std['ard'], df_m['ard'] + df_std['ard'], facecolor = 'C0', alpha=0.3)
    # plt.fill_between(df_m['fg'], df_m['ard'] - df_std['ard']*2, df_m['ard'] + df_std['ard']*2, facecolor = 'C0', alpha=0.3)
    # plt.fill_between(df_m['fg'], df_m['ard'] - df_std['ard']*3, df_m['ard'] + df_std['ard']*3, facecolor = 'C0', alpha=0.3)
    plt.xlabel('VBRK am Sensor [RE]', fontdict = {'size':12})
    plt.ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
    plt.title(f'Gesamte Kalibrierungskurve mit Standardabweichung (Sensor {j})', fontdict = {'size':16}) # alle 9 Messungen als eine Messung zusammen
    plt.xlim(0, 30000)
    plt.ylim(0, 981)
    plt.grid()
    plt.savefig(root[j] + '\Kalibrierungskurve.pdf')
    plt.show()
    
# REPEATABILITY OF MEASUREMENTS

    # drop time column, reset index and join into one dataframe
    df_mess_ges_m = df_mess_ges_m.drop('time', axis = 1).reset_index(drop = True)
    df_mess_ges_s = df_mess_ges_s.drop('time', axis = 1).reset_index(drop = True)
    df_stats = pd.concat([df_mess_ges_m, df_mess_ges_s], axis = 1)
    
    # rename columns and save dataframe as csv
    df_stats.columns = ['ard_m', 'fg_m', 'ard_s', 'fg_s']
    df_stats.to_csv(root[j] + '\Kalibrierungskurve_mit_stat_messw.csv')
    
    # create new dataframes of every measurement
    df_mess_ges_01 = df_stats.iloc[:100,:].reset_index(drop = True)
    df_mess_ges_02 = df_stats.iloc[100:200,:].reset_index(drop = True)
    df_mess_ges_03 = df_stats.iloc[200:300,:].reset_index(drop = True)
    df_mess_ges_04 = df_stats.iloc[300:400,:].reset_index(drop = True)
    df_mess_ges_05 = df_stats.iloc[400:500,:].reset_index(drop = True)
    df_mess_ges_06 = df_stats.iloc[500:600,:].reset_index(drop = True)
    df_mess_ges_07 = df_stats.iloc[600:700,:].reset_index(drop = True)
    df_mess_ges_08 = df_stats.iloc[700:800,:].reset_index(drop = True)
    df_mess_ges_09 = df_stats.iloc[800:900,:].reset_index(drop = True)
    
    # concatenate all dataframes of measurements as new columns, drop time-axis and rename columns
    data_col = ['ard_01_m', 'fg_01_m', 'ard_01_s', 'fg_01_s', 'ard_02_m', 'fg_02_m', 'ard_02_s', 'fg_02_s', 
                'ard_03_m', 'fg_03_m', 'ard_03_s', 'fg_03_s', 'ard_04_m', 'fg_04_m', 'ard_04_s', 'fg_04_s', 
                'ard_05_m', 'fg_05_m', 'ard_05_s', 'fg_05_s', 'ard_06_m', 'fg_06_m', 'ard_06_s', 'fg_06_s',
                'ard_07_m', 'fg_07_m', 'ard_07_s', 'fg_07_s', 'ard_08_m', 'fg_08_m', 'ard_08_s', 'fg_08_s', 
                'ard_09_m', 'fg_09_m', 'ard_09_s', 'fg_09_s']
    df_data = pd.concat([df_mess_ges_01, df_mess_ges_02, df_mess_ges_03, df_mess_ges_04, df_mess_ges_05, 
                         df_mess_ges_06, df_mess_ges_07, df_mess_ges_08, df_mess_ges_09], axis = 1)
    df_data.columns = data_col
    
    # change df_data into Newton for plots 
    for l in range(1, 10):
        df_data[f'fg_0{l}_m'] = df_data[f'fg_0{l}_m']*9.81
        df_data[f'fg_0{l}s'] = df_data[f'fg_0{l}_s']*9.81

    # plot curves of measurements
    plt.figure(figsize = (10, 7))
    # plot sensors and the standard deviation of the measurement as shade
    for l in range(1, 10):
        plt.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
        plt.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                          df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
    plt.xlabel('VBRK am Sensor [RE]', fontdict = {'size':12})
    plt.ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
    plt.title(f'Kalibrierungskurve aller Messungen (Sensor {j})', fontdict = {'size':16})
    plt.xlim(0, 30000)
    plt.ylim(0, 981)
    plt.legend(loc = 2)
    plt.grid(True)
    plt.savefig(root[j] + '\Kalibrierungskurven_Messungen.pdf')
    plt.show()
        
# REGRESSIONCURVES

    # append Zero-row at end and roll to start of dataframe
    df_mess_ges = df_mess_ges.append(pd.Series(0, index = df_mess_ges.columns), ignore_index = True).apply(np.roll, shift = 1)
    
    # sort by ard
    df_mess_ges = df_mess_ges.sort_values('ard').reset_index(drop = True)
        
    # save dataframe in excel
    df_mess_ges.to_csv(root[j] + '\Kalibrierungskurve.csv')
    df_mess_ges.drop(df_mess_ges[df_mess_ges['ard'] > 15000].index, inplace = True)
        
    # # define parameters and function for regression
    x = df_mess_ges['ard']
    y = df_mess_ges['fg']

    # plot of polynomial regression
    plt.figure(figsize = (10, 7))
    plt.scatter(x, y, s = 2, alpha = 0.08, label = 'Messpunkte des Sensors')
    model = np.poly1d(np.polyfit(x, y, 4))
    plt.plot(x, model(x), color = 'black', label = 'Regressionsfunktion')
    plt.xlabel('VBRK am Sensor [RE]', fontdict = {'size':12})
    plt.ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
    plt.title(f'Polynomiale Regression bis 15.000 RE (Sensor {j})', fontdict = {'size':16})
    plt.xlim(0, 15000)
    plt.ylim(0, 350)
    plt.legend(loc = 2)
    plt.grid(True)
    plt.savefig(root[j] + '\Polynomiale_Regression_Sensor.pdf')
    plt.show()

    # save parameters of regression
    df_params = pd.DataFrame(model, columns = [f's{j}'])
    df_params.to_csv(root[j] + '\Regressionsparameter_poly.csv')
    df_reg_params = pd.concat([df_reg_params, df_params], axis = 1)
    
    # concatenate all calibrationcurves in one dataframe as new columns
    df_cal = pd.concat([df_cal, df_mess_ges['ard'], df_mess_ges['fg']], axis = 1)
    
    # append all calibrationcurves in one dataframe as new rows
    df_cal_ges = pd.concat([df_cal_ges, df_mess_ges])
    
    # save df_data of every sensor in list of arrays
    data[j] = (df_data)

# plot all calibrationcurves of every measurements of every sensor together 
fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5), (ax6, ax7)) = plt.subplots(4, 2, figsize = (10, 15), sharex = True, sharey = True)
# plot Sensor 0
ax0 = plt.subplot(4, 2, 1)
for l in range(1, 10):
    df_data = pd.DataFrame(data[0], columns = data_col)
    ax0.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax0.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax0.set_title('Sensor 0', fontdict = {'size':12})
ax0.grid(True)
ax0.set_xlim(0, 30000)
ax0.set_ylim(0, 981)
ax0.set_ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
# plot Sensor 1
ax1 = plt.subplot(4, 2, 2)
for l in range(1, 10):
    df_data = pd.DataFrame(data[1], columns = data_col)
    ax1.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax1.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax1.set_title('Sensor 1', fontdict = {'size':12})
ax1.grid(True)
ax1.set_xlim(0, 30000)
ax1.set_ylim(0, 981)
# plot Sensor 2
ax2 = plt.subplot(4, 2, 3)
for l in range(1, 10):
    df_data = pd.DataFrame(data[2], columns = data_col)
    ax2.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax2.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax2.set_title('Sensor 2', fontdict = {'size':12})
ax2.grid(True)
ax2.set_xlim(0, 30000)
ax2.set_ylim(0, 981)
ax2.set_ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
# plot Sensor 3
ax3 = plt.subplot(4, 2, 4)
for l in range(1, 10):
    df_data = pd.DataFrame(data[3], columns = data_col)
    ax3.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax3.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax3.set_title('Sensor 3', fontdict = {'size':12})
ax3.grid(True)
ax3.set_xlim(0, 30000)
ax3.set_ylim(0, 981)
# plot Sensor 4
ax4 = plt.subplot(4, 2, 5)
for l in range(1, 10):
    df_data = pd.DataFrame(data[4], columns = data_col)
    ax4.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax4.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax4.set_title('Sensor 4', fontdict = {'size':12})
ax4.grid(True)
ax4.set_xlim(0, 30000)
ax4.set_ylim(0, 981)
ax4.set_ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
# plot Sensor 5
ax5 = plt.subplot(4, 2, 6)
for l in range(1, 10):
    df_data = pd.DataFrame(data[5], columns = data_col)
    ax5.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax5.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax5.set_title('Sensor 5', fontdict = {'size':12})
ax5.grid(True)
ax5.set_xlim(0, 30000)
ax5.set_ylim(0, 981)
# plot Sensor 6
ax6 = plt.subplot(4, 2, 7)
for l in range(1, 10):
    df_data = pd.DataFrame(data[6], columns = data_col)
    ax6.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax6.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax6.set_title('Sensor 6', fontdict = {'size':12})
ax6.grid(True)
ax6.set_xlim(0, 30000)
ax6.set_ylim(0, 981)
ax6.set_xlabel('VBRK an den Sensoren [RE]', fontdict = {'size':12})
ax6.set_ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
# plot Sensor 7
ax7 = plt.subplot(4, 2, 8)
for l in range(1, 10):
    df_data = pd.DataFrame(data[7], columns = data_col)
    ax7.plot(df_data[f'ard_0{l}_m'], df_data[f'fg_0{l}_m'], label = f'Messung 0{l}')
    ax7.fill_betweenx((df_data.index + 1)*9.81, df_data[f'ard_0{l}_m'] - df_data[f'ard_0{l}_s'], 
                      df_data[f'ard_0{l}_m'] + df_data[f'ard_0{l}_s'], alpha = 0.3)
ax7.set_title('Sensor 7', fontdict = {'size':12})
ax7.grid(True)
ax7.set_xlim(0, 30000)
ax7.set_ylim(0, 981)
ax7.set_xlabel('VBRK an den Sensoren [RE]', fontdict = {'size':12})
ax7.set_ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
# add legend and save as pdf
plt.subplots_adjust(left = 0.10, bottom = 0.15, right = 0.95, top = 0.93, wspace = 0.15, hspace = 0.15)
h1, l1 = ax0.get_legend_handles_labels()
fig.legend(h1, l1, bbox_to_anchor = [0.5, 0.09], loc = 'center', ncol = 3)
plt.savefig(path + '\Kalibrierungskurven_Sensoren.pdf')
plt.show()


# rename all calibrationcurves
df_cal.columns = ['ard_0', 'fg_0', 'ard_1', 'fg_1', 'ard_2', 'fg_2', 'ard_3', 'fg_3', 
                   'ard_4', 'fg_4', 'ard_5', 'fg_5', 'ard_6', 'fg_6', 'ard_7', 'fg_7']

# sort dataframe by ard, drop time column and ard values
df_cal_ges = df_cal_ges.sort_values(by = ['ard']).reset_index(drop = True)
df_cal_ges = df_cal_ges.drop('time', axis = 1)
df_cal_ges.drop(df_cal_ges[df_cal_ges['ard'] > 15000].index, inplace = True)

# REGRESSIONCURVE (total)

# define parameters and function for regression
x = df_cal_ges['ard']
y = df_cal_ges['fg']
model_ges = np.poly1d(np.polyfit(x, y ,4))

# save all parameters of regression in one dataframe
df_params_ges = pd.DataFrame(model_ges, columns=['sum'])
df_reg_params = pd.concat([df_reg_params, df_params_ges], axis = 1)
df_reg_params.to_csv(path + '\Regressionsparameter_sen.csv')

# plot regression functions (of total and) every sensor
plt.figure(figsize = (10, 7))
# plt.plot(x, model_ges(x), color = 'black', label = 'Polynomiale Regressionkurve aller Sensormessungen')
for u in range(0, 8):
    # dataframe with all datapoints of every sensor
    df_cal_old = pd.concat([df_cal[f'ard_{u}'], df_cal[f'fg_{u}']], axis = 1).dropna()
    df_cal_old = df_cal_old.sort_values(f'ard_{u}')
    df_cal_old.drop(df_cal_old[df_cal_old[f'ard_{u}'] > 15000].index, inplace = True)
    # set old x and y for model and calculate regressioncurve of every sensor again
    x_old = df_cal_old[f'ard_{u}']
    y_old = df_cal_old[f'fg_{u}']
    model = np.poly1d(np.polyfit(x_old, y_old, 4))
    x_ges = np.linspace(0, 15000, 15000)
    plt.plot(x_ges, model(x_ges), label = f'Sensor {u}')
plt.xlabel('VBRK am Sensor [RE]', fontdict = {'size':12})
plt.ylabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
plt.title('Polynomiale Regressionsfunktionen der Sensoren bis 15.000 RE', fontdict = {'size':16})
plt.xlim(0, 15000)
plt.ylim(0, 300)
plt.legend(loc = 2)
plt.grid(True)
plt.savefig(path + '\Polynomiale_Regression_Sensoren.pdf')
plt.show()

# MEAN ABSOLUTE DEVIATION

# calculate MAD of every sensor to their regressioncurve (df_cal)
df_reg_t = pd.DataFrame()
df_reg_tot = pd.DataFrame()
df_mad_reg = pd.DataFrame()
fg = pd.DataFrame()
mad_sen = []
for v in range(0, 8):
    # define dataframe (df_cal_new) as datapoints for MAD
    df_cal_new = pd.concat([df_cal[f'ard_{v}'].dropna(), df_cal[f'fg_{v}'].dropna()], axis = 1)
    df_mad_r = pd.DataFrame(np.nan, index = list(range(len(df_cal_new)-1)), columns = ['0','1', '2', '3', '4', '5', '6', '7'])
    df_mad_abs = pd.DataFrame(np.nan, index = list(range(len(df_cal_new)-1)), columns = ['0','1', '2', '3', '4', '5', '6', '7', '8'])
    # define dataframe of regressioncurve of every sensor (df_reg_tot) at specific fg-values (z_fg)
    z_fg = df_cal[f'ard_{v}'].dropna()
    df_reg_fg = df_reg_params[f's{v}'][0]*z_fg**4 + df_reg_params[f's{v}'][1]*z_fg**3 + df_reg_params[f's{v}'][2]*z_fg**2 + df_reg_params[f's{v}'][3]*z_fg + df_reg_params[f's{v}'][4]
    df_reg_t = pd.concat([df_reg_fg, df_cal[f'ard_{v}'].dropna()], axis = 1) # save fg and ard together
    df_reg_t.columns = [f'fg_{v}', f'ard_{v}']
    df_reg_tot = pd.concat([df_reg_tot, df_reg_t], axis = 1) # concatenate all dataframes into one
    df_reg_tot = df_reg_tot.reset_index(drop = True)
    # calculate residuals, concatenate in dataframe and save as csv
    df_mad_r.iloc[:, v] = df_reg_tot[f'fg_{v}'].dropna() - df_cal_new[f'fg_{v}']
    df_mad_abs.iloc[:, v] = pd.DataFrame(np.abs(df_mad_r[f'{v}'].dropna().values))
    df_mad_reg = pd.concat([df_mad_reg, df_mad_abs[f'{v}']], axis = 1)
    df_mad_reg.to_csv(path + '\Residuals_Sensoren.csv')
    # create x-axis for residuals 
    fg_v = pd.DataFrame(np.arange(0, df_cal_new[f'fg_{v}'].max(), (df_cal_new[f'fg_{v}'].max()/(len(df_mad_reg[f'{v}'].dropna())-0.5))))
    fg = pd.concat([fg, fg_v], axis = 1)
    # calculate MAD of every sensor and save in array
    mad_abs = np.abs(df_mad_reg[f'{v}'].dropna().values).mean()
    mad_sen = np.append(mad_sen, mad_abs)
    df_mad_sen = pd.DataFrame(mad_sen)
    df_mad_sen.to_csv(path + '\MAD_Sensoren.csv')

fg.columns = ['0','1', '2', '3', '4', '5', '6', '7']

# plot residuals of every sensor to their regressioncurve (df_cal)
plt.figure(figsize = (10, 7))
for q in range(0, 8):
    plt.scatter(fg[f'{q}'].dropna(), df_mad_reg[f'{q}'].dropna(), s = 2, alpha = 0.3, label = f'Sensor {q}')
plt.title('Residuale der gemessenen VBRK zu den Regressionsfunktionen', fontdict = {'size':16})
plt.xlabel('VBRK am Kraftmessgerät [N]', fontdict = {'size':12})
plt.ylabel('Residuale [N]', fontdict = {'size':12})
plt.xlim(0, 260) # single sensors have less than 320 Newton in dynamic measurement, but regressioncurves end under 260 Newton
plt.ylim(0, 60)
leg = plt.legend(loc = 2)
for lh in leg.legendHandles: # make legend visible
    lh.set_alpha(1)
plt.grid(True)
plt.savefig(path + '\Residuals_Sensoren.pdf')
plt.show()

# plot MAD of every sensor to their regressioncurve (df_cal)
plt.figure(figsize = (10, 7))
for q in range(0, 8):
    fg = np.arange(0, 8, 1).tolist()
    plt.bar(fg[q], df_mad_sen[0][q], width = 0.5, label = f'Sensor {q}')
    plt.title('MAD der gemessenen VBRKs zu den Regressionsfunktionen', fontdict = {'size':16})
plt.xlabel('Sensor', fontdict = {'size':12})
plt.ylabel('MAD [N]', fontdict = {'size':12})
plt.xlim(-0.5, 7.5)
plt.ylim(0, 15)
plt.grid(True)
plt.savefig(path + '\MAD_Sensoren.pdf')
plt.show()

end_time = datetime.now()
duration = end_time - start_time
print(duration)