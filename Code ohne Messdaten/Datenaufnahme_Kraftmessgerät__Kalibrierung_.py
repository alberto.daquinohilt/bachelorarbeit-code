"""
Created on Tue Nov 24 08:26:54 2022
@author: Alberto d'Aquino Hilt
"""
import time
import serial
import csv
import matplotlib.pyplot as plt 
import os

logdir = '16bit/Messdaten_100kg/'
ser = serial.Serial(port='COM7', 
                    baudrate=9600, 
                    timeout=1, 
                    parity=serial.PARITY_NONE, 
                    stopbits=serial.STOPBITS_ONE, 
                    bytesize=serial.EIGHTBITS)
ser.isOpen()

# Create Timestamp + New Folder 
timestamp = time.asctime(time.localtime())
timestamp = timestamp.replace(':', '-')
directory = logdir + timestamp + '/'
if not os.path.exists(directory):
   os.makedirs(directory)
            
try : 
    messdaten = []           
    messzeiten = []
    time_begin = time.time()
    data = b''
    while 1 :
        bytesToRead = ser.inWaiting()
        new_data = ser.read(bytesToRead)
        data = data + new_data 
        if len(data) == 16 : 
            # decode bytes
            # 3 und 4 bestimmen die Einheit
            # 5 Vorzeichen: Zug = 0 (positiv) und Druck = 1 (negativ)
            # 6 Anzahl Nachkommastellen
            # 7 bis 14 sind die gemessenen Ziffer auf dem Display (8)
            data_string = data.decode()
            nachkommastellen = int(data_string[6])
            ziffer = int(data_string[7:15])
            zahl = ziffer * 10**(-nachkommastellen)
            messdaten.append(zahl)
            messzeiten.append(time.time() - time_begin)
            print(f'Der Messwert ist {zahl:.3f} kg')
            
            with open(directory + f'Messdaten_FG.csv', 'a') as f : 
                writer = csv.writer(f)
                writer.writerow([zahl, time.time()])
                # python csv: wenn in Excel besser anzeigen lassen 
            data = b''
            time.sleep(0.01)
except (Exception, serial.SerialException) as e : 
    print(e)
    ser.close()

except (KeyboardInterrupt) as e : 
    plt.figure(figsize=(10,7))
    plt.plot(messzeiten, messdaten) 
    plt.xlabel('Zeit [s]')
    plt.ylabel('Kilogramm [kg]')
    plt.title('Druckmessung')
    plt.grid(True)
    plt.savefig(directory + f'Plot_FG.png',dpi=250)
    # Liveplots auf https://pythonprogramming.net/live-graphs-matplotlib-tutorial/
    

ser.close()
