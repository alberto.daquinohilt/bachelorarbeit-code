"""
Created on Tue Feb 7 12:16:46 2023
@author: Alberto d'Aquino Hilt
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

start_time = datetime.now()
print(start_time)

# root of code to save total plots
path = r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten'

# root of treadmill and of arduino data
root = [r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 0',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 1',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 2',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 3',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 4',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 5',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 6',
        r'C:\Users\alber\Desktop\Messungen\Dynamische_Messung\Messdaten\Material 7']

# root of dataframe/parameters of regressioncurve
root_reg_sen = r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Regressionsparameter_sen.csv'
root_reg_mat = r'C:\Users\alber\Desktop\Messungen\Statische_Kalibrierung\Messdaten\Regressionsparameter_mat.csv'

# read regressionparameters of all materials and sum
df_reg_mat = pd.read_csv(root_reg_mat, index_col = 0)

# open dataframe of average steps, arduino data and regression data for joined plots
df_time = pd.DataFrame(np.nan, index = list(range(200)), columns = ['ard_0', 'tread_0', 'reg_0', 'ard_1', 'tread_1', 'reg_1',
                                                                    'ard_2', 'tread_2', 'reg_2', 'ard_3', 'tread_3', 'reg_3',
                                                                    'ard_4', 'tread_4', 'reg_4', 'ard_5', 'tread_5', 'reg_5',
                                                                    'ard_6', 'tread_6', 'reg_6', 'ard_7', 'tread_7', 'reg_7'])
df_avg = pd.DataFrame()
df_ard = pd.DataFrame()
df_reg = pd.DataFrame()
df_tread = pd.DataFrame()
df_max = pd.DataFrame()
df_quot = pd.DataFrame()

# iterate through both measurements of every material/root
for j in range(len(root)):
    print(j)
        
    # reset dataframe to empty
    df_ard_l = pd.DataFrame()
    
    # read data of treadmill and save as dataframe
    df_t = pd.read_csv(root[j] + '\\1.amw', sep = ',')
    # save right and left foot as separate dataframes (of 12000 rows)
    df_tread_r = df_t.iloc[:, [2, 3, 4, 5]].sum(axis = 1)
    df_tread_l = df_t.iloc[:, [10, 11, 12, 13]].sum(axis = 1)
        # columns 2, 3, 4 and 5 are the right foot; columns 10, 11, 12 and 13 are the left foot
    
    # calculate treadmill force from Newton into N 
    df_tread_r = df_tread_r
    df_tread_l = df_tread_l
    
    # set all negative values to zero
    for g in range(len(df_tread_l)):
        if (df_tread_l[g] < 0):
            df_tread_r[g] = 0
        if (df_tread_l[g] < 0):
            df_tread_l[g] = 0
    
    # read data of arduino and save as dataframe
    df_ard_l = pd.read_csv(root[j] + '\\1L.csv', sep = ';', header = 1)
    df_ard_r = pd.read_csv(root[j] + '\\1R.csv', sep = ';', header = 1)
    
    # drop all unnecessary columns
    df_ard_l = df_ard_l.drop(columns=['sAx', 'sAy', 'sAz', 'sGx', 'sGy', 'sGz', 'Vib', 'phase', 'classifier', 'steplength_madgwick', 'Pitch', 'points', 'Unnamed: 21'])
    df_ard_r = df_ard_r.drop(columns=['sAx', 'sAy', 'sAz', 'sGx', 'sGy', 'sGz', 'Vib', 'phase', 'classifier', 'steplength_madgwick', 'Pitch', 'points', 'Unnamed: 21'])
    
    # OFFSET of s1
    offset_s1 = df_ard_l['s1'].head(61).mean()
    if (df_ard_l['s1'].head(61).mean() >= 0):
        for i in range(0, len(df_ard_l['s1'])):    # iterate through column 's1'
            if (df_ard_l['s1'][i] - offset_s1 > 0):    # if the offset is lower than value, subtract it
                df_ard_l['s1'][i] = df_ard_l['s1'][i] - offset_s1
            elif (df_ard_l['s1'][i] - offset_s1 < 0):    # if offset is higher than value, set value to 0
                df_ard_l['s1'][i] = 0
    
    # calculate time in seconds and cut everything after 120s and rename time
    df_ard_l['time'] = df_ard_l['time'].div(1000)
    df_ard_l.rename(columns = {'time':f'time_{j}'}, inplace = True)
    
    # plot one step
    if (j == 6):
        plt.figure(figsize = (10, 7))
        df_step = pd.DataFrame(df_ard_l)
        df_step.drop(df_step[df_step[f'time_{j}'] < 79.1].index, inplace = True)
        df_step.drop(df_step[df_step[f'time_{j}'] > 80.3].index, inplace = True)
        for i in range(0, 8):
            plt.plot(df_step[f'time_{j}'], df_step[f's{i}'], label = f'Gemessene VBRK an Sensor {i}')
        plt.xlabel('Zeit [s]', fontdict = {'size':12})
        plt.ylabel('Relative VBRK an den Sensoren [RE]', fontdict = {'size':12})
        plt.title(f'Schritt mit den höchsten VBRKs (Material {j})', fontdict = {'size':16})
        plt.xlim(79.1, 80.3)
        plt.ylim(0, 13000)
        plt.grid()
        plt.legend()
        plt.savefig(path + '\Schritt_Sensoren.pdf')
        plt.show()
        
        # plot sum of all sensor of one step
        plt.figure(figsize = (10, 7))
        df_step.drop(f'time_{j}', axis = 1)
        df_sum = df_step.sum(axis = 1) 
        df_step_sum = pd.concat([df_step[f'time_{j}'], df_sum], axis = 1)
        df_step_sum.columns = ('time', 'sum')
        plt.plot(df_step_sum['time'], df_step_sum['sum'], label = 'Summe der gemessenen VBRKs aller Sensoren')
        plt.xlabel('Zeit [s]', fontdict = {'size':12})
        plt.ylabel('Relative VBRK an den Sensoren [RE]', fontdict = {'size':12})
        plt.title(f'Schritt mit den höchsten VBRKS (Material {j})', fontdict = {'size':16})
        plt.xlim(79.1, 80.3)
        plt.ylim(0, 30000)
        plt.grid()
        plt.legend()
        plt.savefig(path + '\Schritt_Summe.pdf')
        plt.show()
        
    # subtract time shift of list from time of arduino
    time_liste = [4.19, 2.66, 1.8, 1.35, 1.8, 2.12, 1.4, 1.38] # manually taken from plot
    df_ard_l[f'time_{j}'] -= time_liste[j]
    # delete all negative values, all values after 120s and reset index
    df_ard_l.drop(df_ard_l[df_ard_l[f'time_{j}'] < 0].index, inplace = True)
    df_ard_l.drop(df_ard_l[df_ard_l[f'time_{j}'] > 120].index, inplace = True)
    df_ard_l = df_ard_l.reset_index(drop = True)
    
    # set every value of dataframe under 500 to 0
    df_cut = df_ard_l.drop(f'time_{j}', axis = 1)
    df_cut = np.clip(df_cut, 500, None)
    df_cut.replace(500, 0, inplace = True)
    df_ard_l = pd.concat([df_ard_l[f'time_{j}'], df_cut], axis = 1)
    
    # create dataframe with sum of all sensors
    df_ard_l[f'sum_{j}'] = df_ard_l.sum(axis = 1)

# REGRESSION FOR FORCE-PLOTS
    
    # send every value off df_ard_l into regressioncurve, to get N-value of material in new dataframe
    df_reg_l = pd.DataFrame(np.nan, index = list(range(len(df_ard_l))), columns = ['s0', 's1', 's2', 's3', 's4', 's5', 's6', 's7'])
    df_reg_sum = pd.DataFrame()

    # iterate through every column (sensor) and every row
    for k in range(0, 8):
        # calculate regression value of every ard-value of every sensor
        x = df_ard_l[f's{k}']
        df_reg_l[f's{k}'] = df_reg_mat[f'{j}'][0]*x**4 + df_reg_mat[f'{j}'][1]*x**3 + df_reg_mat[f'{j}'][2]*x**2 + df_reg_mat[f'{j}'][3]*x + df_reg_mat[f'{j}'][4]
        
        # get reg_limit for steprecognition
        x = 1000
        reg_limit = df_reg_mat[f'{j}'][0]*x**4 + df_reg_mat[f'{j}'][1]*x**3 + df_reg_mat[f'{j}'][2]*x**2 + df_reg_mat[f'{j}'][3]*x + df_reg_mat[f'{j}'][4]
        
        # set negative values to zero (because of form of regressionfunction)
        df_reg_l[df_reg_l < 0] = 0
        
        # set all values equals regressionfuntion-offset (df_reg_mat[f'{k}'][4]) to zero and save dataframe as csv
        df_reg_l.replace(df_reg_mat[f'{j}'][4], 0, inplace = True)
        df_reg_l.to_csv(root[j] + '\Regressionsdaten.csv')
    
    # calculate sum of regression values
    df_reg_sum = df_reg_l.sum(axis = 1)

# STEPRECOGNITION
        
    # list with 5000 (more than necessary) lists (2d--array)
    step_ard = [[] for i in range(5000)]
    # get average step during stancephase (Belastungsphase) = set stancephase to false and index to zero, to start iteration
    stancephase = False
    ind = 0
    # iterate through df_ard_l['sum']
    for i in range(2, len(df_ard_l)): # -2 steps for good start
        ind += 1
        if df_ard_l[f'sum_{j}'][i] > 1000 and stancephase == False:
            stancephase = True
            ind = 0
        if df_ard_l[f'sum_{j}'][i] < 1000:
            stancephase = False
        # append data to list and as list at increasing index-position
        step_ard[ind].append(df_ard_l[f'sum_{j}'][i-2])
    
    # list with 5000 (more than necessary) lists (2d--array)
    step_tread = [[] for i in range(5000)]
    # get average step during stancephase (Belastungsphase) = set stancephase to false and index to zero, to start iteration
    stancephase = False
    ind = 0
    # iterate through df_tread_l
    for i in range(2, len(df_tread_l)): # -2 steps for good start
        ind += 1
        if df_tread_l[i] > 100 and stancephase == False:
            stancephase = True
            ind = 0
        if df_tread_l[i] < 100:
            stancephase = False
        # append data to list and as list at increasing index-position
        step_tread[ind].append(df_tread_l[i-2])
    
    # list with 5000 (more than necessary) lists (2d--array)
    step_reg = [[] for i in range(5000)]
    # get average step during stancephase (Belastungsphase) = set stancephase to false and index to zero, to start iteration
    stancephase = False
    ind = 0
    limit = reg_limit # limit to define start of step (3-4)
    # iterate through df_reg_sum
    for i in range(2, len(df_reg_sum)): # -2 steps for good start
        ind += 1
        if df_reg_sum[i] > limit and stancephase == False:
            stancephase = True
            ind = 0
        if df_reg_sum[i] < limit:
            stancephase = False
        # append data to list and as list at increasing index-position
        step_reg[ind].append(df_reg_sum[i-2])
        
    # calculate mean of steps and save as dataframe
        # min mean of 10 steps, to be sure not to get noise as step
    average_step_ard = [np.mean(i) for i in step_ard if len(i) > 10] # in N
    df_avg_ard = pd.DataFrame(average_step_ard, columns = [f'avg_a_{j}'])
    average_step_tread = [np.mean(i) for i in step_tread if len(i) > 10] # in N
    df_avg_tread = pd.DataFrame(average_step_tread, columns = [f'avg_t_{j}'])
    average_step_reg = [np.mean(i) for i in step_reg if len(i) > 10] # in N
    df_avg_reg = pd.DataFrame(average_step_reg, columns = [f'avg_r_{j}'])
    
    # calculate time for plots
    df_time[f'ard_{j}'] = pd.DataFrame([i*0.01 for i in range(len(average_step_ard))]) # time of average step for x-axis of plots
    df_time[f'tread_{j}'] = pd.DataFrame([i*0.01 for i in range(len(average_step_tread))]) # time of average step for x-axis of plots
    df_time[f'reg_{j}'] = pd.DataFrame([i*0.01 for i in range(len(average_step_reg))]) # time of regression step for x-axis of plots
    
# PLOTS FOR EVERY MATERIAL
    
    # plot all sensors of arduino
    plt.figure(figsize = (10, 7))
    for o in range(0, 8):
        plt.plot(df_ard_l[f'time_{j}'], df_ard_l[f's{o}'], label = f'Sensor {o}')
    plt.title(f'Gemessenen VBRK an der Einlagesohle (Material {j})', fontdict = {'size':16})
    plt.xlabel('Zeit [s]', fontdict = {'size':12})
    plt.ylabel('Relative VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
    plt.xlim(0, 120)
    plt.ylim(0, 15000)
    plt.grid(True)
    plt.legend(loc = 1)
    plt.savefig(root[j] + '\Rohdaten_Sensoren.pdf')
    plt.show
    # plot sum of arduino
    plt.figure(figsize = (10, 7))
    plt.plot(df_ard_l[f'time_{j}'], df_ard_l[f'sum_{j}'], label = 'VBRK der Summe aller Sensoren')
    plt.title(f'Summe der gemessenen VBRK an der Einlagesohle (Material {j})', fontdict = {'size':16})
    plt.xlabel('Zeit [s]', fontdict = {'size':12})
    plt.ylabel('Relative VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
    plt.xlim(0, 120)
    plt.ylim(0, 30000)
    plt.grid(True)
    plt.legend(loc = 1)
    plt.savefig(root[j] + '\Rohdaten_Summe.pdf')
    plt.show
    
    # plot all sensors of regression
    plt.figure(figsize = (10, 7))
    for l in range(0, 8):
        plt.plot(df_ard_l[f'time_{j}'], df_reg_l[f's{l}'], label = f'Sensor {l}')
    plt.title(f'Regression der gemessenen VBRK an der Einlagesohle (Material {j})', fontdict = {'size':16})
    plt.xlabel('Zeit [s]', fontdict = {'size':12})
    plt.ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
    plt.xlim(0, 120)
    plt.ylim(0, 250)
    plt.grid(True)
    plt.legend(loc = 1)
    plt.savefig(root[j] + '\Regressionsdaten_Sensoren.pdf')
    plt.show
    # plot sum of regression
    plt.figure(figsize = (10, 7))
    plt.plot(df_ard_l[f'time_{j}'], df_reg_sum, label = 'VBRK der Summe aller Sensoren')
    plt.title(f'Regression der Summe der VBRK an der Einlagesohle (Material {j})', fontdict = {'size':16})
    plt.xlabel('Zeit [s]', fontdict = {'size':12})
    plt.ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
    plt.xlim(0, 120)
    plt.ylim(0, 500)
    plt.grid(True)
    plt.legend(loc = 1)
    plt.savefig(root[j] + '\Regressionsdaten_Summe.pdf')
    plt.show
    
    # plot force of insole and treadmill as two y-axis over the time (RE)
    fig, axs = plt.subplots(1, 1, figsize = (10, 7))
    axs.plot(df_ard_l[f'time_{j}'], df_ard_l[f'sum_{j}'], label = 'Einlagesohle')
    axs2 = axs.twinx()
    axs2.plot(np.arange(0, len(df_tread_l)/100, 0.01), df_tread_l, color = 'orange', alpha = 0.7, label = 'Laufband')
    axs.set_xlabel('Zeit [s]', fontdict = {'size':12})
    axs.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
    axs2.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})
    axs.set_xlim(0, 120)
    axs.set_ylim(0, 30000)
    axs2.set_ylim(0, 1875)
    axs.grid(True)
    h1, l1 = axs.get_legend_handles_labels()
    h2, l2 = axs2.get_legend_handles_labels()
    fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.03], loc = 'center', ncol = 2)    
    plt.title(f'Relative VBRK an Einlagesohle und Laufband (Material {j})', fontdict = {'size':16})
    plt.savefig(root[j] + '\Kraftvergleich_RE.pdf')
    plt.show()
    # plot force of insole and treadmill as two y-axis over the time (N)
    fig, axs = plt.subplots(1, 1, figsize = (10, 7))
    axs.plot(df_ard_l[f'time_{j}'], df_reg_sum, label = 'Einlagesohle')
    axs2 = axs.twinx()
    axs2.plot(np.arange(0, len(df_tread_l)/100, 0.01), df_tread_l, color = 'orange', alpha = 0.7, label = 'Laufband')
    axs.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
    axs.set_xlabel('Zeit [s]', fontdict = {'size':12})
    axs2.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})
    axs.set_xlim(0, 120)
    axs.set_ylim(0, 500)
    axs2.set_ylim(0, 1875)
    axs.grid(True)
    h1, l1 = axs.get_legend_handles_labels()
    h2, l2 = axs2.get_legend_handles_labels()
    fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.03], loc = 'center', ncol = 2)    
    plt.title(f'Gemessene VBRK an Einlagesohle und Laufband (Material {j})', fontdict = {'size':16})
    plt.savefig(root[j] + '\Kraftvergleich_N.pdf')
    plt.show()
            
    # plot average_step and df_avg_tread (RE)
    fig, axs = plt.subplots(1, 1, figsize = (10, 7))
    axs.plot(df_time[f'ard_{j}'].dropna(), df_avg_ard[f'avg_a_{j}'], label = 'Einlagesohle')
    axs2 = axs.twinx()
    axs2.plot(df_time[f'tread_{j}'].dropna(), df_avg_tread[f'avg_t_{j}'], color = 'orange', alpha = 0.7, label = 'Laufband')
    axs.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
    axs.set_xlabel('Zeit [s]', fontdict = {'size':12})
    axs2.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})
    axs.set_xlim(0, 1.3)
    axs.set_ylim(0, 25000)
    axs2.set_ylim(0, 1875)
    axs.grid(True)
    h1, l1 = axs.get_legend_handles_labels()
    h2, l2 = axs2.get_legend_handles_labels()
    fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.03], loc = 'center', ncol = 2)    
    plt.title(f'Relativer Gemittelte Schritte (Material {j})', fontdict = {'size':16})
    plt.savefig(root[j] + '\Schritte_RE.pdf')
    plt.show()
# plot average_step and df_avg_tread (N)
    fig, axs = plt.subplots(1, 1, figsize = (10, 7))
    axs.plot(df_time[f'reg_{j}'].dropna(), df_avg_reg[f'avg_r_{j}'], color = 'tab:blue', label = 'Einlagesohle')
    axs2 = axs.twinx()
    axs2.plot(df_time[f'tread_{j}'].dropna(), df_avg_tread[f'avg_t_{j}'], color = 'orange', alpha = 0.7, label = 'Laufband')
    axs.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
    axs.set_xlabel('Zeit [s]', fontdict = {'size':12})
    axs2.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})
    axs.set_xlim(0, 1.3)
    axs.set_ylim(0, 500)
    axs2.set_ylim(0, 1875)
    axs.grid(True)
    h1, l1 = axs.get_legend_handles_labels()
    h2, l2 = axs2.get_legend_handles_labels()
    fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.03], loc = 'center', ncol = 2)    
    plt.title(f'Gemittelte Schritte (Material {j})', fontdict = {'size':16})
    plt.savefig(root[j] + '\Schritte_N.pdf')
    plt.show()
    
# MAXIMUM OF STEPS
    
    # create dataframe for max stepvalues of arduino, treadmill and regression of arduino
    df_step_ard = pd.DataFrame(step_ard)
    df_step_tread = pd.DataFrame(step_tread)
    df_step_reg = pd.DataFrame(step_reg)
    ard_max = []
    tread_max = []
    reg_max = []
    for i in range(0, len(df_step_ard.columns)-1):
        ard_max.append(df_step_ard[i].max())
    for i in range(0, len(df_step_tread.columns)-1):
        tread_max.append(df_step_tread[i].max())
    for i in range(0, len(df_step_reg.columns)-1):
        reg_max.append(df_step_reg[i].max())
    df_ard_max = pd.DataFrame(ard_max)
    df_tread_max = pd.DataFrame(tread_max)
    df_reg_max = pd.DataFrame(reg_max)
    # concatenate maximum of arduino and treadmill, rename columns and drop all 'false' steps
    df_max_mat = pd.concat([df_ard_max, df_tread_max, df_reg_max], axis = 1)
    df_max_mat.columns = [f'ard_{j}', f'tread_{j}', f'reg_{j}']
    df_max_mat.drop(df_max_mat[df_max_mat[f'tread_{j}'] < 10].index, inplace = True)
    df_max_mat = df_max_mat.dropna().reset_index(drop = True)
    
# QUOTIENT OF ARDUINO AND TREADMILL
    
    df_quot_mat = pd.concat([df_max_mat, df_max_mat[f'reg_{j}']/df_max_mat[f'tread_{j}']], axis = 1)
    df_quot_mat.columns = [f'ard_{j}', f'tread_{j}', f'reg_{j}', f'quot_{j}']
    
    # concatenate average steps of every material in dataframe
    df_ard = pd.concat([df_ard, df_ard_l[f'time_{j}'], df_ard_l[f'sum_{j}']], axis = 1)
        
    # concatenate the maximum and quotient of every material and save as csv
    df_max = pd.concat([df_max, df_max_mat], axis = 1)
    df_quot = pd.concat([df_quot, df_quot_mat], axis = 1)
    df_stats = pd.concat([df_max, df_quot], axis = 1)
    df_stats.to_csv(path + '\Maximale Werte und Quotienten jedes Materials.csv')

    df_reg = pd.concat([df_reg, df_reg_sum], axis = 1)
    df_tread = pd.concat([df_tread, df_tread_l], axis = 1)
    df_avg = pd.concat([df_avg, df_avg_ard, df_avg_tread, df_avg_reg], axis = 1)    

# rename columns
df_tread.columns = ['tread_0', 'tread_1', 'tread_2', 'tread_3', 'tread_4', 'tread_5', 'tread_6', 'tread_7']
df_reg.columns = ['0', '1', '2', '3', '4', '5', '6', '7']

# TOTAL JOINED PLOTS

# plot force of insole and treadmill over time, of every material
fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5), (ax6, ax7)) = plt.subplots(4, 2, figsize = (10, 15), sharex = True, sharey = True)

# plot all eight materials
ax0 = plt.subplot(4, 2, 1)
ax0.plot(df_ard['time_0'].dropna(), df_ard['sum_0'].dropna(), color = 'tab:blue', label = 'Einlagesohle')
ax_zero = ax0.twinx()
ax_zero.plot(np.arange(0, len(df_tread[f'tread_{0}'].dropna())/100, 0.01), df_tread[f'tread_{0}'].dropna(), alpha = 0.5, color = 'orange', label = 'Laufband')
ax0.set_title('Material 0', fontdict = {'size':12})
ax0.grid(True)
ax0.set_xlim(0, 120)
ax0.set_ylim(0, 30000)
ax_zero.set_ylim(0, 1875)
ax0.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_zero.axis('off')

ax1 = plt.subplot(4, 2, 2)
ax1.plot(df_ard['time_1'].dropna(), df_ard['sum_1'].dropna(), color = 'tab:blue')
ax_one = ax1.twinx()
ax_one.plot(np.arange(0, len(df_tread[f'tread_{1}'].dropna())/100, 0.01), df_tread[f'tread_{1}'].dropna(), alpha = 0.5, color = 'orange')
ax1.set_title('Material 1', fontdict = {'size':12})
ax1.grid(True)
ax1.set_xlim(0, 120)
ax1.set_ylim(0, 30000)
ax_one.set_ylim(0, 1875)
ax_one.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax2 = plt.subplot(4, 2, 3)
ax2.plot(df_ard['time_2'].dropna(), df_ard['sum_2'].dropna(), color = 'tab:blue')
ax_two = ax2.twinx()
ax_two.plot(np.arange(0, len(df_tread[f'tread_{2}'].dropna())/100, 0.01), df_tread[f'tread_{2}'].dropna(), alpha = 0.5, color = 'orange')
ax2.set_title('Material 2', fontdict = {'size':12})
ax2.grid(True)
ax2.set_xlim(0, 120)
ax2.set_ylim(0, 30000)
ax_two.set_ylim(0, 1875)
ax2.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_two.axis('off')

ax3 = plt.subplot(4, 2, 4)
ax3.plot(df_ard['time_3'].dropna(), df_ard['sum_3'].dropna(), color = 'tab:blue')
ax_three = ax3.twinx()
ax_three.plot(np.arange(0, len(df_tread[f'tread_{3}'].dropna())/100, 0.01), df_tread[f'tread_{3}'].dropna(), alpha = 0.5, color = 'orange')
ax3.set_title('Material 3', fontdict = {'size':12})
ax3.grid(True)
ax3.set_xlim(0, 120)
ax3.set_ylim(0, 30000)
ax_three.set_ylim(0, 1875)
ax_three.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax4 = plt.subplot(4, 2, 5)
ax4.plot(df_ard['time_4'].dropna(), df_ard['sum_4'].dropna(), color = 'tab:blue')
ax_four = ax4.twinx()
ax_four.plot(np.arange(0, len(df_tread[f'tread_{4}'].dropna())/100, 0.01), df_tread[f'tread_{4}'].dropna(), alpha = 0.5, color = 'orange')
ax4.set_title('Material4', fontdict = {'size':12})
ax4.grid(True)
ax4.set_xlim(0, 120)
ax4.set_ylim(0, 30000)
ax_four.set_ylim(0, 1875)
ax4.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_four.axis('off')

ax5 = plt.subplot(4, 2, 6)
ax5.plot(df_ard['time_5'].dropna(), df_ard['sum_5'].dropna(), color = 'tab:blue')
ax_five = ax5.twinx()
ax_five.plot(np.arange(0, len(df_tread[f'tread_{5}'].dropna())/100, 0.01), df_tread[f'tread_{5}'].dropna(), alpha = 0.5, color = 'orange')
ax5.set_title('Material 5', fontdict = {'size':12})
ax5.grid(True)
ax5.set_xlim(0, 120)
ax5.set_ylim(0, 30000)
ax_five.set_ylim(0, 1875)
ax_five.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax6 = plt.subplot(4, 2, 7)
ax6.plot(df_ard['time_6'].dropna(), df_ard['sum_6'].dropna(), color = 'tab:blue')
ax_six = ax6.twinx()
ax_six.plot(np.arange(0, len(df_tread[f'tread_{6}'].dropna())/100, 0.01), df_tread[f'tread_{6}'].dropna(), alpha = 0.5, color = 'orange')
ax6.set_title('Material 6', fontdict = {'size':12})
ax6.grid(True)
ax6.set_xlim(0, 120)
ax6.set_ylim(0, 30000)
ax_six.set_ylim(0, 1875)
ax6.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax6.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_six.axis('off')

ax7 = plt.subplot(4, 2, 8)
ax7.plot(df_ard['time_7'].dropna(), df_ard['sum_7'].dropna(), color = 'tab:blue')
ax_seven = ax7.twinx()
ax_seven.plot(np.arange(0, len(df_tread[f'tread_{7}'].dropna())/100, 0.01), df_tread[f'tread_{7}'].dropna(), alpha = 0.5, color = 'orange')
ax7.set_title('Material 7', fontdict = {'size':12})
ax7.grid(True)
ax7.set_xlim(0, 120)
ax7.set_ylim(0, 30000)
ax_seven.set_ylim(0, 1875)
ax7.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax_seven.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

plt.subplots_adjust(left = 0.10, bottom = 0.15, right = 0.9, top = 0.93, wspace = 0.10, hspace = 0.15)
h1, l1 = ax0.get_legend_handles_labels()
h2, l2 = ax_zero.get_legend_handles_labels()
fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.1], loc = 'center', ncol = 2)
plt.savefig(path + '\Kraftvergleich_RE.pdf')
plt.show()

# plot force of insole and treadmill over time of material sum
fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5), (ax6, ax7)) = plt.subplots(4, 2, figsize = (10, 15), sharex = True, sharey = True)

# plot all eight materials
ax0 = plt.subplot(4, 2, 1)
ax0.plot(df_ard['time_0'].dropna(), df_reg['0'].dropna(), color = 'tab:blue', label = 'Einlagesohle')
ax_zero = ax0.twinx()
ax_zero.plot(np.arange(0, len(df_tread[f'tread_{0}'].dropna())/100, 0.01), df_tread[f'tread_{0}'].dropna(), color = 'orange', alpha = 0.5, label = 'Laufband')
ax0.set_title('Material 0', fontdict = {'size':12})
ax0.grid(True)
ax0.set_xlim(0, 120)
ax0.set_ylim(0, 500)
ax_zero.set_ylim(0, 1875)
ax0.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_zero.axis('off')

ax1 = plt.subplot(4, 2, 2)
ax1.plot(df_ard['time_1'].dropna(), df_reg['1'].dropna(), color = 'tab:blue')
ax_one = ax1.twinx()
ax_one.plot(np.arange(0, len(df_tread[f'tread_{1}'].dropna())/100, 0.01), df_tread[f'tread_{1}'].dropna(), color = 'orange', alpha = 0.5)
ax1.set_title('Material 1', fontdict = {'size':12})
ax1.grid(True)
ax1.set_xlim(0, 120)
ax1.set_ylim(0, 500)
ax_one.set_ylim(0, 1875)
ax_one.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax2 = plt.subplot(4, 2, 3)
ax2.plot(df_ard['time_2'].dropna(), df_reg['2'].dropna(), color = 'tab:blue')
ax_two = ax2.twinx()
ax_two.plot(np.arange(0, len(df_tread[f'tread_{2}'].dropna())/100, 0.01), df_tread[f'tread_{2}'].dropna(), color = 'orange', alpha = 0.5)
ax2.set_title('Material 2', fontdict = {'size':12})
ax2.grid(True)
ax2.set_xlim(0, 120)
ax2.set_ylim(0, 500)
ax_two.set_ylim(0, 1875)
ax2.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_two.axis('off')

ax3 = plt.subplot(4, 2, 4)
ax3.plot(df_ard['time_3'].dropna(), df_reg['3'].dropna(), color = 'tab:blue')
ax_three = ax3.twinx()
ax_three.plot(np.arange(0, len(df_tread[f'tread_{3}'].dropna())/100, 0.01), df_tread[f'tread_{3}'].dropna(), color = 'orange', alpha = 0.5)
ax3.set_title('Material 3', fontdict = {'size':12})
ax3.grid(True)
ax3.set_xlim(0, 120)
ax3.set_ylim(0, 500)
ax_three.set_ylim(0, 1875)
ax_three.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax4 = plt.subplot(4, 2, 5)
ax4.plot(df_ard['time_4'].dropna(), df_reg['4'].dropna(), color = 'tab:blue')
ax_four = ax4.twinx()
ax_four.plot(np.arange(0, len(df_tread[f'tread_{4}'].dropna())/100, 0.01), df_tread[f'tread_{4}'].dropna(), color = 'orange', alpha = 0.5)
ax4.set_title('Material4', fontdict = {'size':12})
ax4.grid(True)
ax4.set_xlim(0, 120)
ax4.set_ylim(0, 500)
ax_four.set_ylim(0, 1875)
ax4.set_ylabel('VBRK an der Einlagesohle [N]')
ax_four.axis('off')

ax5 = plt.subplot(4, 2, 6)
ax5.plot(df_ard['time_5'].dropna(), df_reg['5'].dropna(), color = 'tab:blue')
ax_five = ax5.twinx()
ax_five.plot(np.arange(0, len(df_tread[f'tread_{5}'].dropna())/100, 0.01), df_tread[f'tread_{5}'].dropna(), color = 'orange', alpha = 0.5)
ax5.set_title('Material 5', fontdict = {'size':12})
ax5.grid(True)
ax5.set_xlim(0, 120)
ax5.set_ylim(0, 500)
ax_five.set_ylim(0, 1875)
ax_five.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax6 = plt.subplot(4, 2, 7)
ax6.plot(df_ard['time_6'].dropna(), df_reg['6'].dropna(), color = 'tab:blue')
ax_six = ax6.twinx()
ax_six.plot(np.arange(0, len(df_tread[f'tread_{6}'].dropna())/100, 0.01), df_tread[f'tread_{6}'].dropna(), color = 'orange', alpha = 0.5)
ax6.set_title('Material 6', fontdict = {'size':12})
ax6.grid(True)
ax6.set_xlim(0, 120)
ax6.set_ylim(0, 500)
ax_six.set_ylim(0, 1875)
ax6.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax6.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_six.axis('off')

ax7 = plt.subplot(4, 2, 8)
ax7.plot(df_ard['time_7'].dropna(), df_reg['7'].dropna(), color = 'tab:blue')
ax_seven = ax7.twinx()
ax_seven.plot(np.arange(0, len(df_tread[f'tread_{7}'].dropna())/100, 0.01), df_tread[f'tread_{7}'].dropna(), color = 'orange', alpha = 0.5)
ax7.set_title('Material 7', fontdict = {'size':12})
ax7.grid(True)
ax7.set_xlim(0, 120)
ax7.set_ylim(0, 500)
ax_seven.set_ylim(0, 1875)
ax7.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax_seven.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

plt.subplots_adjust(left = 0.10, bottom = 0.15, right = 0.9, top = 0.93, wspace = 0.10, hspace = 0.15)
h1, l1 = ax0.get_legend_handles_labels()
h2, l2 = ax_zero.get_legend_handles_labels()
fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.1], loc = 'center', ncol = 2)
plt.savefig(path + '\Kraftvergleich_N.pdf')
plt.show()


# plot average step of every material into one plot
fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5), (ax6, ax7)) = plt.subplots(4, 2, figsize = (10, 15), sharex = True, sharey = True)

#plot all eight materials
ax0 = plt.subplot(4, 2, 1)
ax0.plot(df_time['ard_0'].dropna(), df_avg['avg_a_0'].dropna(), color = 'tab:blue', label = 'Einlagesohle')
ax_zero = ax0.twinx()
ax_zero.plot(df_time['tread_0'].dropna(), df_avg['avg_t_0'].dropna(), color = 'orange', label = 'Laufband')
ax0.set_title('Material 0', fontdict = {'size':12})
ax0.grid(True)
ax0.set_xlim(0, 1.3)
ax0.set_ylim(0, 30000)
ax_zero.set_ylim(0, 1800)
ax0.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_zero.axis('off')

# plot materials 1 to 8
ax1 = plt.subplot(4, 2, 2)
ax1.plot(df_time['ard_1'].dropna(), df_avg['avg_a_1'].dropna(), color = 'tab:blue')
ax_one = ax1.twinx()
ax_one.plot(df_time['tread_1'].dropna(), df_avg['avg_t_1'].dropna(), color = 'orange')
ax1.set_title('Material 1', fontdict = {'size':12})
ax1.grid(True)
ax1.set_xlim(0, 1.3)
ax1.set_ylim(0, 30000)
ax_one.set_ylim(0, 1800)
ax_one.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax2 = plt.subplot(4, 2, 3)
ax2.plot(df_time['ard_2'].dropna(), df_avg['avg_a_2'].dropna(), color = 'tab:blue')
ax_two = ax2.twinx()
ax_two.plot(df_time['tread_2'].dropna(), df_avg['avg_t_2'].dropna(), color = 'orange')
ax2.set_title('Material 2', fontdict = {'size':12})
ax2.grid(True)
ax2.set_xlim(0, 1.3)
ax2.set_ylim(0, 30000)
ax_two.set_ylim(0, 1800)
ax2.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_two.axis('off')

ax3 = plt.subplot(4, 2, 4)
ax3.plot(df_time['ard_3'].dropna(), df_avg['avg_a_3'].dropna(), color = 'tab:blue')
ax_three = ax3.twinx()
ax_three.plot(df_time['tread_3'].dropna(), df_avg['avg_t_3'].dropna(), color = 'orange')
ax3.set_title('Material 3', fontdict = {'size':12})
ax3.grid(True)
ax3.set_xlim(0, 1.3)
ax3.set_ylim(0, 30000)
ax_three.set_ylim(0, 1800)
ax_three.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax4 = plt.subplot(4, 2, 5)
ax4.plot(df_time['ard_4'].dropna(), df_avg['avg_a_4'].dropna(), color = 'tab:blue')
ax_four = ax4.twinx()
ax_four.plot(df_time['tread_4'].dropna(), df_avg['avg_t_4'].dropna(), color = 'orange')
ax4.set_title('Material 4', fontdict = {'size':12})
ax4.grid(True)
ax4.set_xlim(0, 1.3)
ax4.set_ylim(0, 30000)
ax_four.set_ylim(0, 1800)
ax4.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_four.axis('off')

ax5 = plt.subplot(4, 2, 6)
ax5.plot(df_time['ard_5'].dropna(), df_avg['avg_a_5'].dropna(), color = 'tab:blue')
ax_five = ax5.twinx()
ax_five.plot(df_time['tread_5'].dropna(), df_avg['avg_t_5'].dropna(), color = 'orange')
ax5.set_title('Material 5', fontdict = {'size':12})
ax5.grid(True)
ax5.set_xlim(0, 1.3)
ax5.set_ylim(0, 30000)
ax_five.set_ylim(0, 1800)
ax_five.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax6 = plt.subplot(4, 2, 7)
ax6.plot(df_time['ard_6'].dropna(), df_avg['avg_a_6'].dropna(), color = 'tab:blue')
ax_six = ax6.twinx()
ax_six.plot(df_time['tread_6'].dropna(), df_avg['avg_t_6'].dropna(), color = 'orange')
ax6.set_title('Material 6', fontdict = {'size':12})
ax6.grid(True)
ax6.set_xlim(0, 1.3)
ax6.set_ylim(0, 30000)
ax_six.set_ylim(0, 1800)
ax6.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax6.set_ylabel('VBRK an der Einlagesohle [RE]', fontdict = {'size':12})
ax_six.axis('off')

ax7 = plt.subplot(4, 2, 8)
ax7.plot(df_time['ard_7'].dropna(), df_avg['avg_a_7'].dropna(), color = 'tab:blue')
ax_seven = ax7.twinx()
ax_seven.plot(df_time['tread_7'].dropna(), df_avg['avg_t_7'].dropna(), color = 'orange')
ax7.set_title('Material 7', fontdict = {'size':12})
ax7.grid(True)
ax7.set_xlim(0, 1.3)
ax7.set_ylim(0, 30000)
ax_seven.set_ylim(0, 1800)
ax7.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax_seven.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

plt.subplots_adjust(left = 0.10, bottom = 0.15, right = 0.9, top = 0.93, wspace = 0.10, hspace = 0.15)
h1, l1 = ax0.get_legend_handles_labels()
h2, l2 = ax_zero.get_legend_handles_labels()
fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.1], loc = 'center', ncol = 2)
plt.savefig(path + '\Schritte_RE.pdf')
plt.show()

# plot average step of every material into one plot
fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5), (ax6, ax7)) = plt.subplots(4, 2, figsize = (10, 15), sharex = True, sharey = True)

# plot Ausgangsmaterial
ax0 = plt.subplot(4, 2, 1)
ax0.plot(df_time['reg_0'].dropna(), df_avg['avg_r_0'].dropna(), color = 'tab:blue', label = 'Einlagesohle')
ax_zero = ax0.twinx()
ax_zero.plot(df_time['tread_0'].dropna(), df_avg['avg_t_0'].dropna(), color = 'orange', label = 'Laufband')
ax0.set_title('Material 0', fontdict = {'size':12})
ax0.grid(True)
ax0.set_xlim(0, 1.3)
ax0.set_ylim(0, 400)
ax_zero.set_ylim(0, 2000)
ax0.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_zero.axis('off')

# plot materials 1 to 8
ax1 = plt.subplot(4, 2, 2)
ax1.plot(df_time['reg_1'].dropna(), df_avg['avg_r_1'].dropna(), color = 'tab:blue')
ax_one = ax1.twinx()
ax_one.plot(df_time['tread_1'].dropna(), df_avg['avg_t_1'].dropna(), color = 'orange')
ax1.set_title('Material 1', fontdict = {'size':12})
ax1.grid(True)
ax1.set_xlim(0, 1.3)
ax1.set_ylim(0, 400)
ax_one.set_ylim(0, 2000)
ax_one.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax2 = plt.subplot(4, 2, 3)
ax2.plot(df_time['reg_2'].dropna(), df_avg['avg_r_2'].dropna(), color = 'tab:blue')
ax_two = ax2.twinx()
ax_two.plot(df_time['tread_2'].dropna(), df_avg['avg_t_2'].dropna(), color = 'orange')
ax2.set_title('Material 2', fontdict = {'size':12})
ax2.grid(True)
ax2.set_xlim(0, 1.3)
ax2.set_ylim(0, 400)
ax_two.set_ylim(0, 2000)
ax2.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_two.axis('off')

ax3 = plt.subplot(4, 2, 4)
ax3.plot(df_time['reg_3'].dropna(), df_avg['avg_r_3'].dropna(), color = 'tab:blue')
ax_three = ax3.twinx()
ax_three.plot(df_time['tread_3'].dropna(), df_avg['avg_t_3'].dropna(), color = 'orange')
ax3.set_title('Material 3', fontdict = {'size':12})
ax3.grid(True)
ax3.set_xlim(0, 1.3)
ax3.set_ylim(0, 400)
ax_three.set_ylim(0, 2000)
ax_three.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax4 = plt.subplot(4, 2, 5)
ax4.plot(df_time['reg_4'].dropna(), df_avg['avg_r_4'].dropna(), color = 'tab:blue')
ax_four = ax4.twinx()
ax_four.plot(df_time['tread_4'].dropna(), df_avg['avg_t_4'].dropna(), color = 'orange')
ax4.set_title('Material 4', fontdict = {'size':12})
ax4.grid(True)
ax4.set_xlim(0, 1.3)
ax4.set_ylim(0, 400)
ax_four.set_ylim(0, 2000)
ax4.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_four.axis('off')

ax5 = plt.subplot(4, 2, 6)
ax5.plot(df_time['reg_5'].dropna(), df_avg['avg_r_5'].dropna(), color = 'tab:blue')
ax_five = ax5.twinx()
ax_five.plot(df_time['tread_5'].dropna(), df_avg['avg_t_5'].dropna(), color = 'orange')
ax5.set_title('Material 5', fontdict = {'size':12})
ax5.grid(True)
ax5.set_xlim(0, 1.3)
ax5.set_ylim(0, 400)
ax_five.set_ylim(0, 2000)
ax_five.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

ax6 = plt.subplot(4, 2, 7)
ax6.plot(df_time['reg_6'].dropna(), df_avg['avg_r_6'].dropna(), color = 'tab:blue')
ax_six = ax6.twinx()
ax_six.plot(df_time['tread_6'].dropna(), df_avg['avg_t_6'].dropna(), color = 'orange')
ax6.set_title('Material 6', fontdict = {'size':12})
ax6.grid(True)
ax6.set_xlim(0, 1.3)
ax6.set_ylim(0, 400)
ax_six.set_ylim(0, 2000)
ax6.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax6.set_ylabel('VBRK an der Einlagesohle [N]', fontdict = {'size':12})
ax_six.axis('off')

ax7 = plt.subplot(4, 2, 8)
ax7.plot(df_time['reg_7'].dropna(), df_avg['avg_r_7'].dropna(), color = 'tab:blue')
ax_seven = ax7.twinx()
ax_seven.plot(df_time['tread_7'].dropna(), df_avg['avg_t_7'].dropna(), color = 'orange')
ax7.set_title('Material 7', fontdict = {'size':12})
ax7.grid(True)
ax7.set_xlim(0, 1.3)
ax7.set_ylim(0, 400)
ax_seven.set_ylim(0, 2000)
ax7.set_xlabel('Zeit [s]', fontdict = {'size':12})
ax_seven.set_ylabel('VBRK am Laufband [N]', fontdict = {'size':12})

plt.subplots_adjust(left = 0.10, bottom = 0.15, right = 0.9, top = 0.93, wspace = 0.10, hspace = 0.15)
h1, l1 = ax0.get_legend_handles_labels()
h2, l2 = ax_zero.get_legend_handles_labels()
fig.legend([*h1, *h2], [*l1, *l2], bbox_to_anchor = [0.5, 0.1], loc = 'center', ncol = 2)
plt.savefig(path + '\Schritte_N.pdf')
plt.show()

# plot average treadmill step of material in %
plt.figure(figsize = (10, 7))
plt.plot(df_time['tread_0'].dropna()*80, df_avg['avg_t_0'].dropna(), label = 'Material 0')
plt.xlabel('Schrittzyklus [%]', fontdict = {'size':12})
plt.ylabel('VBRK am Laufband [N]', fontdict = {'size':12})
plt.title('Schrittzyklus am instrumentierten Laufband', fontdict = {'size':16})
plt.xlim(0, 100)
plt.ylim(0, 2000)
plt.grid()
plt.legend()
plt.savefig(path + '\Schritt_Laufband_Material_0.pdf')
plt.show()

# calculate duration of program
end_time = datetime.now()
duration = end_time - start_time
print(duration)
