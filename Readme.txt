Im Ordner "Code ohne Messdaten" befinden sich die Python-Programme. Diese können eingesehen, jedoch nicht ausgeführt werden.

In der Zip "Messungen" befinden sich sowohl alle Python-Programme, als auch die Messdaten, CSV-Dateien und Plots der Ergebnisse.
Für die Ausführung muss die Zip heruntergeladen und entpackt werden. Die Python-Programme befinden sich in den passenden Unterordnern und sollten nicht verschoben werden, da die Funktion des Codes ansonsten nicht garantiert ist.
Vor der Ausführung der Programme muss außerdem darauf geachtet werden "path" und "root" der Programme an den jeweiligen Speicherort auf dem lokalen PC anzupassen, diese befinden sich direkt am Anfang des Codes.

Bei Fragen oder Problemen jederzeit an "alberto.daquinohilt@stud.tu-darmstadt.de" wenden.